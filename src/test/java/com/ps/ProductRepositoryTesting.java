package com.ps;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.ps.model.Brand;
import com.ps.model.Category;
import com.ps.model.Product;
import com.ps.repo.ProductRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class ProductRepositoryTesting {
	
	@Autowired
	private ProductRepository repo;
	
	@Autowired
	private TestEntityManager entityManger;
	
	@Test
	@Disabled
	public void testCreateProduct() {
		Brand brand = entityManger.find(Brand.class,7);
		Category category=entityManger.find(Category.class,4);
		
       Product product=new Product();
       product.setName("Iphone x");
       product.setAlias("Iphone x brand");
       product.setShortDescription("A good SmartPhone from Iphone");
       product.setFullDescription("This is a very good SmartPhone full Description");
       
       product.setBrand(brand);
       product.setCategory(category);
       
       product.setPrice(456);
       product.setCost(400);
       product.setEnabled(true);
       product.setInStock(true);
       product.setCreateDate(new Date());
       product.setUpdatedDate(new Date());
       
       Product saveProduct=repo.save(product);
       assertThat(saveProduct).isNotNull();
       assertThat(saveProduct.getId()).isGreaterThan(0);
		
	}
	
//	@Test
//	@Disabled
//	public void testAllProducts() {
//	    List<Product> prods = repo.getAllProducts();
////		Iterator<Product> iterator = prods.iterator();
////		
////		while(iterator.hasNext()) {
////			Product next = iterator.next();
////			System.out.println("products"+next);
////		}
//	    prods.forEach(ele->{
//	    	System.out.println("element"+ele);
//	    });
//		
////		IntStream.range(0,prods.size()).forEach(element->
////			System.out.println(prods)
////		);
//		
//	}
	
	@Test
	@Disabled
	public void testGetProduct() {
		Integer id=2;
		Product product=repo.findById(id).get();
		System.out.println(product);
		assertThat(product).isNotNull();
		
	}
	
	
	
	@Test
	@Disabled
	public void testUpdateProduct() {
		Integer id=2;
		Product product=repo.findById(id).get();
		System.out.println(product);
		product.setPrice(333);
		repo.save(product);
		Product updateProduct = entityManger.find(Product.class, 2);
		assertThat( updateProduct.getPrice()).isEqualTo(333);
	}
	
	

	@Test
	@Disabled
	public void testDeleteProduct() {
		Integer id=2;
		repo.deleteById(id);
	    Optional<Product> result=repo.findById(id);
		assertThat(!result.isPresent());
	}
	
	
	@Test
	@Disabled
	public void testSaveProductImage() {
		Integer productId=1;
		Product product=repo.findById(productId).get();
		product.setMainImage("main image.jpg");
		product.addExtreaImages("extra_image1.jpg");
		product.addExtreaImages("extra_image2.png");
		product.addExtreaImages("extra_image3.png");
		
		Product savedProduct = repo.save(product);
		assertThat(savedProduct.getImages().size()).isEqualTo(3);
	}
	
	@Test
	public void testSaveProductDetails() {
		Integer productId=1;
		Product product=repo.findById(productId).get();
		product.addDetails("Device Memory","128 GB");
		product.addDetails("CPU Model","MediaTesk");
		product.addDetails("OS","Android");
		
		Product productSave=repo.save(product);
		assertThat(productSave.getDetails()).isNotEmpty();
	}
	

}

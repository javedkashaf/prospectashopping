package com.ps;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.matches;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.ps.model.Brand;
import com.ps.model.Category;
import com.ps.model.MainBrand;
import com.ps.repo.BrandRepository;
import com.ps.repo.CategoryRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class BrandTestRepository {

	@Autowired
	private BrandRepository repo;
	
	@Autowired
	private CategoryRepository catRepo;
	
	@Test
	@Disabled
	public void testCreateBrand1() {
//		Category laptops = new Category(1);
		
		Category cate=catRepo.findById(3).get();
		
		Brand acer = new Brand("Mercedecs");
		acer.setBrandAlias(acer.getName());
		acer.getCategories().add(cate);
		
		Brand savedBrand = repo.save(acer);
		assertThat(savedBrand).isNotNull();
		assertThat(savedBrand.getId()).isGreaterThan(0);
	}
	
	
	
	
	@Test
	@Disabled
	public void testCreateBrand7() {
//		Category laptops = new Category(1);
		
		
		Category cate=catRepo.findById(3).get();
		Brand acer = new Brand("Mercedecs");
		acer.setBrandAlias(acer.getName());
		acer.getCategories().add(cate);
		
		
		
		Brand savedBrand = repo.save(acer);
		assertThat(savedBrand).isNotNull();
		assertThat(savedBrand.getId()).isGreaterThan(0);
	}
	
	
	@Test
	@Disabled
	public void testCreateBrand2() {
		
		Category carModel = new Category(1);
		Category Computer = new Category(2);
		
		Brand machinery = new Brand("Grapes");
		machinery.setBrandAlias(machinery.getName());
		machinery.getCategories().add(Computer);
		machinery.getCategories().add(carModel);
		
		Set<MainBrand> storySet = new HashSet<>();
		
		MainBrand brand1 = new MainBrand();
		brand1.setBrandName("MainBrand1");
		brand1.setBrandAlias("Fresh Brand 1");
		brand1.setCategoryName("Data Communication");
		brand1.setBrand(machinery);
		
		MainBrand brand2 = new MainBrand();
		brand2.setBrandName("MainBrand2");
		brand2.setBrandAlias("Fresh Brand2");
		brand2.setCategoryName("Data Communication 2");
		brand2.setBrand(machinery);
		
		
		MainBrand brand3 = new MainBrand();
		brand3.setBrandName("MainBrand3");
		brand3.setBrandAlias("Fresh Brand3");
		brand3.setCategoryName("Data Communication 3");
		brand3.setBrand(machinery);
		
		
		storySet.add(brand3);
		storySet.add(brand2);
		storySet.add(brand1);
		machinery.setMainBrand(storySet);

		Brand saveBrand = repo.save(machinery);
		
		assertThat(saveBrand).isNotNull();
		assertThat(saveBrand.getId()).isGreaterThan(0);
	}
	
	
	@Test
	@DisplayName("FIND ALL BRAND")
	public void testFindAll() {
		Iterable<Brand> brands=repo.findAll();
		brands.forEach(element->{
	System.out.println("brand"+element);
		});
	}
	
	@Test
	@Disabled
	public void testGetById() {
		Brand brand=repo.findById(1).get();
		assertThat(brand.getName()).isEqualTo("Acer");
	}
	
	@Test
	@Disabled
	public void testUpdateName() {
		String newName = "computer";
		Brand acer=repo.findById(1).get();
		acer.setName(newName);
		
		Brand saveBrand = repo.save(acer);
		assertThat(saveBrand.getName()).isEqualTo(newName);
	}
	
	@Test
	@Disabled
	public void testDelete() {
		Integer id=1;
		repo.deleteById(id);
		Optional<Brand> result = repo.findById(id);
		assertThat(result.isEmpty());
	}
	
	
	


}

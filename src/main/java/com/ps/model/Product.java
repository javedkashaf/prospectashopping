package com.ps.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PRODUCTS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(unique = true,length = 256,name = "name")
	private String name;

	@Column(length = 256,name = "alias")
	private String alias;
	
	@Column(length = 256,name = "short_description")
	private String shortDescription;

	@Column(length = 4096,name = "full_description")
	private String fullDescription;
	
	private boolean enabled;
	
	@Column(name="in_stock")
	private boolean inStock;
	
	private float cost;
	
	private float price;
	
	@Column(name="discount_percent")
	private float discountPercent;
	
	
	private float length;
	private float width;
	private float height;
	private float weight;
	
	@Column(name="main_image")
	private String mainImage;
	
	@ManyToOne
	@JoinColumn(name="brand_id")
	private Brand brand;
	
	@ManyToOne
	@JoinColumn(name="category_Id",referencedColumnName = "id")
	private Category category;
	
	@Column(name = "productCreated")
	private Date productCreated;
	
	@Column(name = "updateProduct")
	private Date productUpate;
	
	
	@OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
	private Set<ProductImage> images=new HashSet<>();
	
	@OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
	private Set<ProductDetail> details = new HashSet<>();
	
	public void addExtreaImages(String imageName) {
		this.images.add(new ProductImage(imageName,this));
	}
	
	
	public void addDetails(String name,String value) {
		this.details.add(new ProductDetail(name,value,this));
	}
	
	
	public boolean containsImageName(String imageName) {
		Iterator<ProductImage> iterator = images.iterator();
		
		while (iterator.hasNext()) {
			ProductImage image = iterator.next();
			if (image.getName().equals(imageName)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	

	
//	@Transient
//	public String getMainImagePage() {
//		if(id == null || mainImage == null) {
//			return "/images/image-thumbnail.png";
//		}
//		return "/product-images"+this.id +"/" + this.mainImage;
//	}
	
	@Transient
	public String getShortName() {
		if (name.length() > 3) {
			return name.substring(0,3).concat("...");
		}
		return name;
	}

	
	@JsonIgnore
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", updatable = false)
	private Date createDate;

	@JsonIgnore
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", insertable = false)
	private Date updatedDate;

	

	
}

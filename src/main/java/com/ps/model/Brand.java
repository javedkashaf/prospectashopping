package com.ps.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "brands")
public class Brand {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;
	
	@Column(length = 45)
	private String name;
	
	@Column(length = 45)
	private String BrandAlias;
	
	@Column(nullable = false)
	private boolean enabled=true;
	
	@Column(length = 128)
	private String logo;
	
	@ManyToMany
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(
			name = "brands_categories",
			joinColumns = @JoinColumn(name = "brand_id"),
			inverseJoinColumns = @JoinColumn(name = "category_id")
			)
	private Set<Category> categories = new HashSet<>();
	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="brand", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("brand")
	private Set<MainBrand> mainBrand = new HashSet<>();

	
	public Brand() {
		
	}

	public Set<MainBrand> getMainBrand() {
		return mainBrand;
	}

	public void setMainBrand(Set<MainBrand> mainBrand) {
		this.mainBrand = mainBrand;
	}

	public Brand(String name) {
		this.name = name;
		this.logo = "brand-logo.png";
	}
	
	public Brand(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public String getBrandAlias() {
		return BrandAlias;
	}

	public void setBrandAlias(String brandAlias) {
		BrandAlias = brandAlias;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


//	@Override
//	public String toString() {
//		return "Brand [id=" + id + ", name=" + name + ", BrandAlias=" + BrandAlias + ", enabled=" + enabled + ", logo="
//				+ logo + ", categories=" + categories + ", mainBrand=" + mainBrand + "]";
//	}


	
	
	
}

package com.ps.model;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="MainBrand")
public class MainBrand {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="brand_Name",length = 156)
	private String brandName; 
	
	@Column(name="brand_Alias",length = 156)
	private String brandAlias;
	
	@Column(name="categoryName",length = 156)
	private String categoryName;
	
	
	@Column(name="picture",length = 156)
	private String picture;
	
	@Column(name="enabled")
	private boolean enabled=true;
	
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "brand_id")
	@JsonIgnoreProperties("mainBrand")
	private Brand brand;
	
	


	public MainBrand(String brandName) {
		this.brandName = brandName;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getBrandName() {
		return brandName;
	}


	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}


	public String getBrandAlias() {
		return brandAlias;
	}


	public void setBrandAlias(String brandAlias) {
		this.brandAlias = brandAlias;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public String getPicture() {
		return picture;
	}


	public void setPicture(String picture) {
		this.picture = picture;
	}


	public boolean isEnabled() {
		return enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public Brand getBrand() {
		return brand;
	}


	public void setBrand(Brand brand) {
		this.brand = brand;
	}


	@Override
	public String toString() {
		return "MainBrand [id=" + id + ", brandName=" + brandName + ", brandAlias=" + brandAlias + ", categoryName="
				+ categoryName + ", picture=" + picture + ", enabled=" + enabled + ", brand=" + brand + "]";
	}


	public MainBrand() {
	}
	
	
	
	
	
	
	
	

}

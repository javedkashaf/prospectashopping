package com.ps.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ps.model.MainBrand;

public interface MainBrandRepository extends JpaRepository<MainBrand, Integer>  {
	
	    @Query(value = "SELECT * FROM main_brand WHERE brand_id = :id", nativeQuery = true)
	    public List<MainBrand> BrandId(Integer id);

}

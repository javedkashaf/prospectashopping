package com.ps.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ps.model.Product;
import com.ps.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	public User findByFirstName(String firstName);

	@Query("SELECT u FROM User u WHERE u.email = :email")
	public User getUserByEmail(@Param("email") String email);
	
	public User findByEmail(String email);
		
	public List<User> findAll();
	
	   @Query(value = "SELECT u FROM User u JOIN u.roles r WHERE"
	   		+ " (u.firstName = '' or u.firstName LIKE %:firstName%) "
	   		+ "or (u.lastName = '' or u.lastName LIKE %:lastName%) or"
	   		+ " (u.phoneNumber = '' or u.phoneNumber LIKE %:phoneNumber%) or"
	   		+ " (r.name = '' or r.name LIKE %:name%)"
	   		)
	   List<User> getAllUsers(@Param("firstName") String firstName,
			   @Param("lastName") String lastName,
			   @Param("phoneNumber") String phoneNumber,
			   @Param("name") String name
			   );
	   
	   
	   @Query(value = "SELECT u FROM User u JOIN u.roles r  WHERE"
	   		+ " (u.firstName = '' or u.firstName LIKE %:firstName%) "
	   		+ "or (u.lastName = '' or u.lastName LIKE %:lastName%) or"
	   		+ " (u.phoneNumber = '' or u.phoneNumber LIKE %:phoneNumber%) or "
	   		+ " (r.name = '' or r.name LIKE %:name%) or "
	   		+ "(u.enabled = :enabled)"
	   		)
	   List<User> getAllUsersWithBoolean(@Param("firstName") String firstName,
			   @Param("lastName") String lastName,
			   @Param("phoneNumber") String phoneNumber,
			   @Param("name") String name,
			   @Param("enabled") boolean enabled
			   );
	   
	   
//	   @Query(value="SELECT u,rl.name FROM User u left join users_roles as r on r.user_id=u.id left join roles_new as rl on rl.id=r.role_id WHERE (rl.name = '' or rl.name LIKE '%Admin%');")
	   @Query("SELECT u FROM User u JOIN u.roles r WHERE r.name LIKE %?1%")
	   List<User> getAllShipperData(@Param("role") String role );
	   
//	   SELECT u.*,rl.name FROM users_new u left join users_roles as r on r.user_id=u.id left join roles_new as rl on rl.id=r.role_id WHERE (u.first_name = '' or rl.name LIKE '%shipper%') or (u.last_name = '' or u.last_name LIKE '%kumar%') ;
	
	

}

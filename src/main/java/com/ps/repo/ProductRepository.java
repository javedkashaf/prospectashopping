package com.ps.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ps.model.Product;
import com.ps.model.User;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{
	
	
	   @Modifying
	   @Query("UPDATE Product p SET p.enabled = ?2 WHERE p.id = ?1")
	   public int updateProductActivate( Integer id,boolean enabled);
	   
//	   @Query(value="SELECT * FROM products",nativeQuery=true)
//	   @Query(value = "SELECT p FROM Product p")
//	   @Query("select p from Product p "
//		          + "where (:name='' or p.name=:name) "
//		          + "or (:alias='' or p.alias=:alias) "
//		          + "or (:cost='' or p.cost=:cost)"
//		          + "or (:price='' or p.price=:price)"
//			   )
	   
	   
	   
	   @Query(value = "SELECT p FROM Product p JOIN p.category c JOIN p.brand b WHERE (c.name='' or c.name LIKE %?1%) "
	   		+ "or (c.name='' or c.name LIKE %?2%)" )
	   Page<Product> getAllProducts(
			   String category,
			   String brand,Pageable pageable
			   );
//			   @Param("alias") String alias,
//			   @Param("cost") float cost,
//			   @Param("price") float price);
	   
	   
//	   @Query(value = "SELECT u FROM User u JOIN u.roles r WHERE"
//		   		+ " (u.firstName = '' or u.firstName LIKE %:firstName%) "
//		   		+ "or (u.lastName = '' or u.lastName LIKE %:lastName%) or"
//		   		+ " (u.phoneNumber = '' or u.phoneNumber LIKE %:phoneNumber%) or"
//		   		+ " (r.name = '' or r.name LIKE %:name%)"
//		   		)
//		   List<User> getAllUsers(@Param("firstName") String firstName,
//				   @Param("lastName") String lastName,
//				   @Param("phoneNumber") String phoneNumber,
//				   @Param("name") String name
//				   );
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   @Query("SELECT prod.name FROM Product prod WHERE prod.name  = :name")
	    String findProductName(@Param("name") String name);
	   
	   public Integer countById(Integer id);
	   


}

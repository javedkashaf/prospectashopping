package com.ps.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ps.model.Brand;
import com.ps.model.Category;

public interface BrandRepository extends JpaRepository<Brand, Integer> {
	
	public Long countById(Integer id);
	
	public Brand findByName(String name);
	
	
//	@Query("SELECT b FROM Brand b WHERE b.name LIKE %?1%")
//	public Page<Brand> findAll(String keyword, Pageable pageable);
//	
//	@Query("SELECT NEW Brand(b.id, b.name) FROM Brand b ORDER BY b.name ASC")
//	public List<Brand> findAll();
	
	@Query("SELECT NEW Brand (b.id,b.name) FROM Brand b ORDER BY b.name ASC")
	public List<Brand> findBrands();
	
	
	 @Query(value = "SELECT b FROM Brand b WHERE"
		   		+ " (b.name = '' or b.name LIKE  %:name%) "
		   		+ "or (b.BrandAlias = '' or b.BrandAlias LIKE %:BrandAlias%)"
		   		)
		   List<Brand> getAllBrands(
				   @Param("name") String name,
				   @Param("BrandAlias") String BrandAlias
				   );
		   
		   
		  
		   
		   @Query(value = "SELECT b FROM Brand b WHERE"
			   		+ " (b.name = '' or b.name LIKE %:name%) or "
			   		+ "(b.BrandAlias = '' or b.BrandAlias LIKE %:BrandAlias%) or"
			   		+ "(b.enabled = :enabled)"
			   		)
		   List<Brand> getAllBrandsWithBoolean(
				   @Param("name") String name,
				   @Param("BrandAlias") String BrandAlias,
				   @Param("enabled") boolean enabled
				   );
}

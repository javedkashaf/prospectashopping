package com.ps.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ps.exception.ProductNotFoundException;
import com.ps.model.Product;
import com.ps.model.User;
import com.ps.repo.DeleteRepository;
import com.ps.repo.ProductRepository;
import com.ps.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository prodRepo;
	
	
	@Autowired
	private DeleteRepository deleteRepo;


	private Long countById;

	@Override
	public Product SaveProduct(Product product) {
		if(product.getId() == null) {
			product.setProductCreated(new Date());
		}
		if(product.getAlias() == null || product.getAlias().isEmpty()) {
			String defaultAlias=product.getName().replaceAll(" ","-");
		}else {
			product.setAlias(product.getAlias().replaceAll(" ", "-"));
		}
		product.setProductUpate(new Date());
		
		return prodRepo.save(product);
	}

	@Override
	public Product getOneProduct(Integer id) {
		Product product=null;
		Optional<Product> opt=prodRepo.findById(id);
		if(opt.isPresent()) {
			product=opt.get();
		}
		return product;
	}

	@Override
	public void deleteById(Integer id) {
		prodRepo.deleteById(id);
		
	}

	@Override
	public boolean isProductExist(Integer id) {
		prodRepo.existsById(id);
		return true;
	}

	@Override
	public Integer updateProduct(Product product) {
		return prodRepo.save(product).getId();
	}

	@Override
	public List<Product> getByMultipleIds(Integer[] ids) {
		List<Product> list=deleteRepo.findByIdIn(ids);
		return list;
	}

	@Override
	public String checkUniques(String name) {
		return "OK";
	}

	@Override
	@Transactional
	public void EnabledAndDisbaledProduct(Integer id, boolean enabled) {
		 prodRepo.updateProductActivate(id,enabled);
	}

	@Override
	public List<Product> products() {
		return (List<Product>) prodRepo.findAll();
	}

	@Override
	public void deleteProduct(Integer id) throws ProductNotFoundException {
		 Integer countById = prodRepo.countById(id);
		if(countById == null || countById == 0) {
			throw new ProductNotFoundException("Could not found any product with Id:"+id);
		}
		 prodRepo.deleteById(id);
	return;
	}

	@Override
	public void saveProductPrice(Product productInform) {
		Product productInDb=prodRepo.findById(productInform.getId()).get();
		productInDb.setCost(productInform.getCost());
		productInDb.setPrice(productInform.getPrice());
		productInDb.setDiscountPercent(productInform.getDiscountPercent());
		
	}

	@Override
	public Page<Product> getAllSearchProducts(String name,String brand,int pageNo,int pageSize) {
		Pageable paging = PageRequest.of(pageNo, pageSize);
		Page<Product> pagedResult = prodRepo.getAllProducts(name,brand,paging);
		return pagedResult;
	}

//	@Override
//	public List<Product> getAllProducts(String name, String alias, float cost, float price) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
//	@Override
//	@Transactional
//	public int updateProductStatus(boolean status, Integer id) {
//		return prodRepo.updateProductActivate(status, id);
//	}
	
//	@Override
//	public boolean deleteWelcomeByIds(Integer[] ids) {
//		Page<Product> list=prodRepo.findByIdIn(ids);
//		prodRepo.deleteInBatch(list);
//		return true;
//	}

	

}

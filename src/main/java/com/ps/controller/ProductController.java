package com.ps.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ps.exception.ProductNotFoundException;
import com.ps.exception.UserNotFoundException;
import com.ps.model.Brand;
import com.ps.model.Category;
import com.ps.model.Product;
import com.ps.model.ProductDetail;
import com.ps.model.ProductImage;
import com.ps.repo.BrandRepository;
import com.ps.repo.ProductRepository;
import com.ps.service.CategoryJson;
import com.ps.service.FilesStorageService;
import com.ps.service.ProductJson;
import com.ps.service.ProductService;
import com.ps.serviceImpl.DeleteService;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService ProdService;
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private DeleteService deleteService;
	
	 @Autowired
	  private FilesStorageService storageService;
	
	@Autowired
	private BrandRepository brandRepo;
	
	private static Logger logger = LoggerFactory.getLogger(ProductController.class);

	
//	@GetMapping("/alls")
//	public ResponseEntity<Map<String,Object>> getAllData(@RequestParam (required = false) String name,@RequestParam (required = false) String alias,
//			@RequestParam(required = false) float cost,@RequestParam(required = false) float price){
//	    try {
//	    	
//	    	System.out.println("name:"+name);
//	    	System.out.println("alias:"+alias);
//	    	System.out.println("cost:"+cost);
//	    	System.out.println("price:"+price);
//	    	
//	    	
//	    	ArrayList<Product> PushProduct=new ArrayList<>();
//	       List<Product> allProducts = ProdService.getAllProducts(name,alias,cost,price);
//	       System.out.println(allProducts);
//	       
//	      Map<String, Object> response = new HashMap<>();
//	      response.put("list",allProducts);
//	      return new ResponseEntity<>(response,HttpStatus.OK);
//	    } catch (Exception e) {
//	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//	    }
//	  }
	
	
	
	
	
	
	
	
	
	
	
	@GetMapping("/all")
	public ResponseEntity<Map<String,Object>> getProducts(){
	    try {
	    	ArrayList<Product> PushProduct=new ArrayList<>();
	       List<Product> allProducts = ProdService.products();
	       
//	       allProducts.forEach(ele->{
//	    	  PushProduct.add(ele);
//	       });
	       
//	       int[] result = IntStream.range(0, a.length).map(i -> a[i] * b[i]).toArray();

//	       PushProduct = allProducts.stream()
//	               .filter(elt -> elt != null)
//	               .forEach(elt -> PushProduct.add(elt));
	       
	       List<Product> collect = allProducts.stream()
	    	        .filter(elt -> elt != null)
	    	        .map(elt -> (elt))
	    	        .collect(Collectors.toList()); 
	       
	      Map<String, Object> response = new HashMap<>();
	      response.put("list",collect);
	      return new ResponseEntity<>(response,HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	//get brand by name
	
	@GetMapping("/brands")
	public ResponseEntity<List<Brand>> getBrands(){

		ResponseEntity<List<Brand>> resp=null;
		Map<String,Object> map= new HashMap<>();
		List<Brand> findBrands = brandRepo.findBrands();
		
//		Map<Object, List<Brand>> multipleFieldsMapPair = findBrands.stream()
//		.collect(Collectors.groupingBy(e -> Pair.of(e.getName(), e.getId())));
		
//		Map<String, Map<Integer, List<Brand>>> multipleFieldsMapList = findBrands.stream()
//		        .collect(
//		                Collectors.groupingBy(Brand::getName, 
//		                        Collectors.groupingBy(Brand::getId)));
		
//		List<Object> set = findBrands.stream()
//				  .flatMap(p -> Stream.of(p.getId(),p.getName()))
//				  .collect(Collectors.toList());
		
		
		
		map.put("success", true);
		map.put("brands",findBrands);
		
		return resp= new ResponseEntity(map,HttpStatus.OK);
		
	}
	
	
    //2.save all usesrs
	@PostMapping("/save")
	public ResponseEntity<?> saveroductDetails(@RequestParam(name="selectedfiles",required=false) MultipartFile[] multipartFiles,
			@RequestPart("product") String product,@RequestParam(name="file") MultipartFile multipartFile) throws Exception {
		ResponseEntity<?> resp = null; 
		
		  Product products = ProductJson.getJson(product);
	try {
		if(multipartFiles.length > 0) {
			 for(int i=0;i<multipartFiles.length;i++){
            	 String fileName = StringUtils.cleanPath(multipartFiles[i].getOriginalFilename());
				  String uploadDir="user-photo";
				  products.addExtreaImages(fileName);
		          storageService.save(uploadDir, fileName, multipartFiles[i]);
             }
		}
			if(!multipartFile.isEmpty()) {
				  String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
				  String uploadDir="user-photo";
			      storageService.save(uploadDir, fileName, multipartFile);
				resp= new ResponseEntity<>(("Product Image is Saved succeffylly"),HttpStatus.OK);	
			}
		if(products!=null) {
			  String productName = productRepo.findProductName(products.getName());
			  System.out.println(product);
			if(productName != null) {
				return resp= new ResponseEntity<>("Product Already exit",HttpStatus.INTERNAL_SERVER_ERROR);	
			}
			
			if(products.getDetails().size() > 0) {
				
				for(int i=0;i<products.getDetails().size();i++) {
//					Set<ProductDetail> details = products.getDetails();
//					Iterator<ProductDetail> iterator = details.iterator();
//					while (iterator.hasNext()) {
//						ProductDetail next = iterator.next();
//						String name=next.getName();
//						String value=next.getValue();
//						products.addDetails(name,value);
//					}
				}
			}
		
//			if(products.getDetails().size() > 0) {
//				products.getDetails().forEach(ele->{
//					String name=ele.getName();
//					String value=ele.getValue();
//					
//					System.out.println("name:"+name);
//					System.out.println("value:"+value);
//					products.addDetails(name,value);
//				});
//			}
			
		 Product saveProduct = ProdService.SaveProduct(products);
		return resp= new ResponseEntity<>("Product Saved Successfully",HttpStatus.CREATED);	
		}
	} catch (Exception e) {
	e.printStackTrace();
	}
	return null;
	}
	

	//3.get one user
	@GetMapping("/{id}")
	public ResponseEntity<?> getProductDetails(@PathVariable(required = true) Integer id) {
		System.out.println();
		ResponseEntity<String> resp = null;

		try {
			Product  prodId= ProdService.getOneProduct(id);
			if (prodId!=null) {
				return new ResponseEntity<Product>(prodId, HttpStatus.OK);
			}
			else {
				return new ResponseEntity<String>("No Product exist with id:"+id,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			throw  new UserNotFoundException("please try after some time");	
		}		
	}
	

	//5.update one user
	@PutMapping
	public ResponseEntity<?> updateProduct(@RequestParam(name="selectedfiles",required=false) MultipartFile[] multipartFiles,
			@RequestPart("product") String product,@RequestParam(name="file",required=false) MultipartFile multipartFile) throws Exception{
		logger.info("Update product called!");
		
		ResponseEntity<?> resp = null; 
		
		  Product products = ProductJson.getJson(product);
		  logger.info("class change to the their data types");
			Integer productId=products.getId();
			boolean productExist = ProdService.isProductExist(productId);
			
			Product  prodId= ProdService.getOneProduct(productId);
			if(productExist == false) { 
				logger.debug("enter to the method of data found!");
			return resp=new ResponseEntity<String>("Product not found by this "+productId,HttpStatus.INTERNAL_SERVER_ERROR);
		}

	try {
		if(multipartFiles != null) {
			 for(int i=0;i<multipartFiles.length;i++){
				 logger.info("checking the for the multipart files");
          	 String fileName = StringUtils.cleanPath(multipartFiles[i].getOriginalFilename());
				  String uploadDir="user-photo";
				  if (!products.containsImageName(fileName)) {
					  logger.info("checking the for the multipart files"+prodId);
						  products.addExtreaImages(fileName);
				          storageService.save(uploadDir, fileName, multipartFiles[i]);
					}
			
           }
		}
		
		
		if(multipartFiles == null) {
	   logger.warn("multipart files is not exist");
		System.out.println("nothing exist in all photos");
		}
		
		if(multipartFile != null) {
			 logger.warn("multipart files exist");
			  String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
			  String uploadDir="user-photo";
		      storageService.save(uploadDir, fileName, multipartFile);
		      logger.info("image saved successfully"+prodId);
			resp= new ResponseEntity<>(("Product Image is Saved succeffylly"),HttpStatus.OK);	
		}
		
		if(multipartFile == null) {
			 logger.warn("one image is exist for the employee"+prodId);
			products.setMainImage(prodId.getMainImage());
		}
		 logger.info("product all data saved with Id:"+prodId);
			  String productName = productRepo.findProductName(products.getName());
			  System.out.println(product);
		      Product saveProduct = ProdService.SaveProduct(products);
		      logger.trace("product saved successfully!:"+prodId);
		return resp= new ResponseEntity("Product Updated Successfully",HttpStatus.CREATED);	
	
	} catch (Exception e) {
		  logger.error("product not saved properly!:",e);
	throw new Exception(e.getMessage());
	}
		
	}
	
	
	//10.for status update
			//activate
			@GetMapping("/activate/{id}")
			public ResponseEntity<String> activateUser(@PathVariable Integer id)
			{
				ResponseEntity<String> resp=null;
				productRepo.updateProductActivate(id,true);
				return resp=new ResponseEntity<String>("Activated Succefully",HttpStatus.OK);
			}
			
			//11.inactive
			@GetMapping("/inactive/{id}")
			public ResponseEntity<String> deActivateUser(@PathVariable Integer id)
			{
				ResponseEntity<String> resp=null;
				productRepo.updateProductActivate(id,false);
				return resp=new ResponseEntity<String>("Deactivated Succefully!!! ",HttpStatus.OK);
			}
				
			
				// 10. Export One row to PDF File
//				@GetMapping("pdf/{id}")
//				public ModelAndView exportOnePdf(@PathVariable Integer[] id) {
//					ModelAndView m = new ModelAndView();
//					m.setView(new ProductPdfView());
//					m.addObject("list",ProdService.getByMultipleIds(id));
//					return m;
//				}
				
//				 @GetMapping("/customers.xlsx/{id}")
//				    public ResponseEntity excelCustomersReport(@PathVariable Integer[] id) throws IOException {
//				        List<Product> products = ProdService.getByMultipleIds(id);
//						
//						ByteArrayInputStream in = ExcelNewView.customersToExcel(products);
//						// return IOUtils.toByteArray(in);
//						
//						HttpHeaders headers = new HttpHeaders();
//				        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");
//						
//						 return ResponseEntity
//					                .ok()
//					                .headers(headers)
//					                .body(new InputStreamResource(in));
//				    }
			
			//delete product 
			@DeleteMapping("/{id}")
			public ResponseEntity<?> deleteProduct(@PathVariable(name = "id")Integer id) throws ProductNotFoundException {
			ResponseEntity<?> resp=null;
			System.out.println("product Id:"+id);
			try {
			if(id != null && id!= 0) {
			ProdService.deleteProduct(id);
			resp=new ResponseEntity<String>("Product Deleted Successfully with Id:"+id,HttpStatus.OK);
			}
			} catch (ProductNotFoundException e) {
			 throw new ProductNotFoundException("Pleace contact with Amin and Try later");
			}
			return resp;	
			}	
			
			
			
			//save pictures to db
			@PostMapping("/savefile")
			public ResponseEntity<?> savePicture(@RequestParam(name="file") MultipartFile multipartFile) {
				ResponseEntity<?> resp = null;
			try {
				if(!multipartFile.isEmpty()) {
					  String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
					  String uploadDir="user-photo";
				      storageService.save(uploadDir, fileName, multipartFile);
					resp= new ResponseEntity<>(("Product Image is Saved succeffylly"),HttpStatus.OK);	
				}
			} catch (Exception e) {
			throw new UserNotFoundException("Product alraedy exist");
			}
			return resp;
			}	
			
			    @PostMapping("/uploadfile")
			    public ResponseEntity<String> handlefileupload(@RequestParam("selectedfiles") MultipartFile[] multifile){
			    	 Map<String, MultipartFile> filemap=new HashMap<String, MultipartFile>();
			        String message="";
			        ResponseEntity<String> resp = null;
			        try {
			            message="succesfull";
			            for(int i=0;i<multifile.length;i++){
			            	 String fileName = StringUtils.cleanPath(multifile[i].getOriginalFilename());
							  String uploadDir="user-photo";
//						      storageService.save(uploadDir, fileName, multifile[i]);
			             }
			            resp= new ResponseEntity<>(("Product Image is Saved Successfully!"),HttpStatus.OK);	
			        } catch (Exception e) {
			            e.printStackTrace();
			            message="failed";
			            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
			        }
			        return resp;
			    }
			    
			    //getProduct Details
				@GetMapping("/model/{id}")
				public ResponseEntity<?> getProductDetailsModel(@PathVariable(required = true) Integer id) {
					System.out.println();
					ResponseEntity<String> resp = null;

					try {
						Product  prodId= ProdService.getOneProduct(id);
						if (prodId!=null) {
							return new ResponseEntity<Product>(prodId, HttpStatus.OK);
						}
						else {
							return new ResponseEntity<String>("No Product exist with id:"+id,HttpStatus.INTERNAL_SERVER_ERROR);
						}
						
					} catch (Exception e) {
						throw  new UserNotFoundException("please try after some time");	
					}		
				}
				
				@GetMapping("/search")
				public ResponseEntity<Map<String,Object>> getAllSearch(
						@RequestParam (required = false) String name,
						@RequestParam (required = false) String brand,
						@RequestParam (required = false) int pageNo,
						@RequestParam (required = false) int pageSize
						){

//					@GetMapping("/countries/{pageNo}/{pageSize}")
//				    public List<Country> getPaginatedCountries(@PathVariable int pageNo, 
//				            @PathVariable int pageSize) {
//
//				        return countryService.findPaginated(pageNo, pageSize);
				    try {
				    	
				    	ArrayList<Product> PushProduct=new ArrayList<>();
				    	if(pageNo == 0 && pageSize == 0) {
				    		pageNo=0;
				    		pageSize=20;
				    	}
				       Page<Product> allProducts = ProdService.getAllSearchProducts(name,brand,pageNo, pageSize);	       
//				       List<Product> collect = allProducts.stream()
//				       .filter(elt -> elt != null)
//				       .map(elt -> (elt))
//				       .collect(Collectors.toList()); 
				      Map<String, Object> response = new HashMap<>();
				      response.put("list",allProducts);
				      return new ResponseEntity<>(response,HttpStatus.OK);
				    } catch (Exception e) {
				      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
				    }
				  }
			    

			    
		
			
			
}

package com.ps.controller;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ps.dto.CategoryDTO;
import com.ps.exception.BrandNotFoundException;
import com.ps.exception.UserNotFoundException;
import com.ps.model.Brand;
import com.ps.model.Category;
import com.ps.model.MainBrand;
import com.ps.model.User;
import com.ps.repo.MainBrandRepository;
import com.ps.response.BrandNotFoundRestException;
import com.ps.response.UserResponse2;
import com.ps.service.BrandService;
import com.ps.service.FilesStorageService;
import com.ps.serviceImpl.FilesStorageServiceImpl;

@RestController
@RequestMapping("/brand")
public class BrandRestController {
	
	@Autowired
	private BrandService service;
	
	 @Autowired
	  FilesStorageService storageService;
	 
	 @Autowired
	 private MainBrandRepository mainbrand;
	 
	 @Autowired
	 private FilesStorageServiceImpl storageServiceImpl;
	
	@PostMapping("/brands/check_unique")
	public String checkUniques(@Param("id")Integer id,@Param("name")String name) {
		return service.checkUniques(id, name);
	}
	
	@GetMapping("/brands")
	public ResponseEntity<?> ListAllBrands() {
		Map<String,Object> res = new HashMap<>();
		ResponseEntity<?> resp = null;
		List<Brand> listAll = service.listAll();
		res.put("data", listAll);
		res.put("success", true);
		return resp=new ResponseEntity<>(res,HttpStatus.OK);
		
	}
	

	
	
	@GetMapping("/brands/{id}/categories")
	public ResponseEntity<CategoryDTO> listCategoriesByBrand(@PathVariable(name = "id") Integer brandId) throws BrandNotFoundRestException{
		ResponseEntity<CategoryDTO> resp=null;
		Map<String,Object> map=new HashMap<>();
		List<CategoryDTO> listCategories=new ArrayList<>();
		try {
			Brand brand=service.getoneBrand(brandId);
			Set<Category> categories = brand.getCategories();
			for(Category category:categories) {
				CategoryDTO dto=new CategoryDTO(category.getId(),category.getName());
				listCategories.add(dto);
			}
			map.put("categories",listCategories);
			map.put("success",true);
			return resp=new ResponseEntity(map,HttpStatus.OK);
		} catch (BrandNotFoundException e) {
			throw new BrandNotFoundRestException();
		}
	}
	
	
	
	
	

	
	
    //2.save all usesrs
	@PostMapping("/save")
	public ResponseEntity<?> saveBrand(@RequestBody() Brand brand) {
		
		ResponseEntity<?> resp = null;
	try {
	   
		if(brand.getId()==null) {
			Brand id=service.save(brand);
			
//			brand.getMainBrand().forEach((element)->{
//				element.setBrand(brand);
//			});
  		    
			for (MainBrand mbrand : brand.getMainBrand()) {
				 MainBrand mbrandc = new MainBrand();
				mbrandc.setBrandAlias(mbrand.getBrandAlias());
				mbrandc.setBrandName(mbrand.getBrandName());
				mbrandc.setCategoryName(mbrand.getCategoryName());
				mbrandc.setEnabled(mbrand.isEnabled());
				mbrandc.setPicture(mbrand.getPicture());
				mbrandc.setBrand(id);
				mainbrand.save(mbrandc);
			}
			resp= new ResponseEntity<>(("Brand is Saved succeffylly"),HttpStatus.OK);	
		}
		else {
			Integer brandId=brand.getId();
			if(service.isUserBrand(brandId)) {
				Brand id=service.UpdateBrand(brand);
				resp=new ResponseEntity<String>("Brand Updated Successfully"+brand,HttpStatus.OK);
			}
		}
	} catch (Exception e) {
	throw new UserNotFoundException("Brand Already save "+brand.getName());
	}
	return resp;
	}
	
	
	
	
	   //2.save all usesrs
		@PostMapping("/update")
		public ResponseEntity<?> UpdateBrand(@RequestBody() Brand brand) {
			System.out.println(brand);
			ResponseEntity<?> resp = null;
		try {
			if(brand.getId() != null){
				Integer brandId=brand.getId();
				if(service.isUserBrand(brandId)) {
//					if(brand.getMainBrand().isEmpty()) {
//						Brand id=service.UpdateBrand(brand);
//					}else {
//						Brand id=service.UpdateBrand(brand);
//					}
					
//					 List<MainBrand> brandIds = service.getBrandIds(brandId);
					
//					System.out.println("data"+brandIds);
					
//					brand.getMainBrand().forEach((element)->{
//					element.setId(element.getId());	
//					element.setBrand(brand);
//				});
					
					Brand id=service.UpdateBrand(brand);
					
										
					resp=new ResponseEntity<String>("Brand Updated Successfully"+brand,HttpStatus.OK);
				}else {
					resp=new ResponseEntity<String>("Brand Not Exist by this Name"+brand,HttpStatus.OK);
				}
			}else {
				resp=new ResponseEntity<String>("Brand Id is Not Exist"+brand,HttpStatus.OK);
			}
		} catch (Exception e) {
		throw new UserNotFoundException("User Already save "+brand.getName());
		}
		return resp;
		}
	
	//2.save all usesrs
		@PostMapping("/savefile")
		public ResponseEntity<?> savePicture(@RequestParam(name="file") MultipartFile multipartFile) {
			ResponseEntity<?> resp = null;
		try {
			if(!multipartFile.isEmpty()) {
				  String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
				  String uploadDir="user-photo";
			      storageService.save(uploadDir, fileName, multipartFile);
				resp= new ResponseEntity<>(("file saved successfully"),HttpStatus.OK);	
			}
		} catch (Exception e) {
		throw new UserNotFoundException("Image not saved successfully");
		}
		return resp;
		}
		
		
		
		@GetMapping("/{id}")
		public ResponseEntity<?> getBrandDetail(@PathVariable(required = true) Integer id) {
			ResponseEntity<String> resp = null;
			try {
				Brand  brand= service.getoneBrand(id);
				if (brand!=null) {
					Map<String,Object> data = new HashMap<>();
					data.put("success", true);
					data.put("data", brand);
					
					return new ResponseEntity(data, HttpStatus.OK);
				}
				else {
					return new ResponseEntity<String>("No brand exist with id:"+id,HttpStatus.INTERNAL_SERVER_ERROR);
				}
				
			} catch (Exception e) {
				throw  new UserNotFoundException("please try after some time");	
			}
		}
		
		
		
		@GetMapping("/delete/{id}")
		public ResponseEntity<?> deleteBrand(@PathVariable(name = "id") Integer id) {
			ResponseEntity<String> resp = null;
			try {
				if(id != null) {
			Brand  brand= service.getoneBrand(id);
			System.out.println(brand.getLogo());
			service.delete(id);
			String brandDir = "user-photo/"+brand.getLogo() ;
			storageServiceImpl.removeDir(brandDir);
			   resp= new ResponseEntity<String>("brand deleted Sucessfully!!", HttpStatus.OK);
				}else {
					resp=new ResponseEntity<String>("brand Id is not Exist!!", HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} catch (BrandNotFoundException ex) {
				resp= new ResponseEntity<String>("brand deleted Sucessfully!!", HttpStatus.ACCEPTED);
			}
			return resp;
		}	
		
		
		@GetMapping("/search")
		public ResponseEntity<?> listBySearch(
				@RequestParam (required = false) String name,
				@RequestParam (required = false) String BrandAlias,
				@RequestParam (required = false)  String enabled
				){ 
			String Inactive="InActive";
			String active="Active";
			  try {
				  List<Brand> list=new ArrayList<>();
				
				  if(enabled != null) {
					  if(!enabled.isEmpty()) {
						  if(Inactive.equals(enabled)){
								Boolean Inactive1=false;
							     list = service.getAllSearchWithBoolean(name,BrandAlias,Inactive1); 
						  }
						  if(enabled.equals(active)) {
							  Boolean active1=true;
							     list = service.getAllSearchWithBoolean(name,BrandAlias,active1);
						  }  
						  
					  }
					  else {
						     list = service.getAllSearch(name,BrandAlias);
					  }
				  }
			      return new ResponseEntity<>(list, HttpStatus.OK);
			    } catch (Exception e) {
			      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			    }
			  }		
}

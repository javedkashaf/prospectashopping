package com.ps.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.ps.model.Brand;

@RestController
@RequestMapping("/client")
public class WebClientController {
	

//	@Autowired
//	public RestTemplate template;
	
	@Autowired
	private WebClient.Builder webClientBuilder;
	
	
//	@GetMapping("/Touristes")
//	public ResponseEntity<?> getAllTouristes() throws Exception, JsonProcessingException{
//		
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<?> resp=null;
//		
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/findAll";
//		
//		ResponseEntity<String> response = template.exchange(serviceUrl,HttpMethod.GET,null,String.class);
//		String JsonResp = response.getBody();
//		ObjectMapper mapper=new ObjectMapper();
//		List<Touristes> readValue = mapper.readValue(JsonResp, new TypeReference<List<Touristes>>() {
//		});
//		map.put("data", readValue);
//		return resp = new ResponseEntity<>(map,HttpStatus.OK);
//	}
//	
//	
//	@GetMapping("/Touriest/{id}")
//	public ResponseEntity<Touristes> getOneTouriest(@PathVariable Integer id) throws JsonMappingException, JsonProcessingException{ 
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<Touristes> resp=null;
//		
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/find/{id}";
//		ResponseEntity<String> response = template.exchange(serviceUrl, HttpMethod.GET,null,String.class,id);
//		String jsonRes=response.getBody();
//		ObjectMapper mapper= new ObjectMapper();
//		Touristes readValue = mapper.readValue(jsonRes, Touristes.class);
//		map.put("Edit", readValue);
//		return resp=new ResponseEntity(map,HttpStatus.OK);
//		
//	}
//	
//	@DeleteMapping("/Touriest/{id}")
//	public ResponseEntity<Touristes> DeleteOneTouriest(@PathVariable Integer id) throws JsonMappingException, JsonProcessingException{ 
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<Touristes> resp=null;
//		
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/delete/{id}";
//		ResponseEntity<String> response = template.exchange(serviceUrl, HttpMethod.DELETE,null,String.class,id);
//		String jsonRes=response.getBody();
////		ObjectMapper mapper= new ObjectMapper();
////		Touristes readValue = mapper.readValue(jsonRes, Touristes.class);
//		map.put("message", jsonRes);
//		return resp=new ResponseEntity(map,HttpStatus.OK);
//		
//	}
//	
//	@PostMapping("/Tourists")
//	public ResponseEntity<String> saveTourists(@RequestBody Touristes touistes) throws JsonProcessingException{
//		ObjectMapper mapper=new ObjectMapper();
//		String jsonData = mapper.writeValueAsString(touistes);
//		
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<String> resp=null;
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/register";
//		HttpHeaders header=new HttpHeaders();
//		header.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> entity=new HttpEntity<String>(jsonData,header);
//		ResponseEntity<String> response = template.exchange(serviceUrl, HttpMethod.POST, entity, String.class);
//		String body = response.getBody();
//		return resp=new ResponseEntity<String>(body,HttpStatus.CREATED);
//		
//	}
//	
//	
//	@PutMapping("/updateTouriest")
//	public ResponseEntity<String> UpdateTourists(@RequestBody Touristes touistes) throws JsonProcessingException{
//		ObjectMapper mapper=new ObjectMapper();
//		String jsonData = mapper.writeValueAsString(touistes);
//		
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<String> resp=null;
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/modify";
//		HttpHeaders header=new HttpHeaders();
//		header.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> entity=new HttpEntity<String>(jsonData,header);
//		ResponseEntity<String> response = template.exchange(serviceUrl, HttpMethod.PUT, entity, String.class);
//		String body = response.getBody();
//		return resp=new ResponseEntity<String>(body,HttpStatus.CREATED);
//		
//	}
	
	
//	@GetMapping("/Touriest")
//	public ResponseEntity<?> AllTourests(){
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/findAll";
//		ResponseEntity<?> resp=null;
//		Touristes[] touriests = webClientBuilder.build()
//		.get()
//		.uri(serviceUrl)
//		.retrieve()
//		.bodyToMono(Touristes[].class)
//		.block();
//		return resp=new ResponseEntity(touriests,HttpStatus.ACCEPTED);
//		
//	}
	
	
//	@GetMapping("/Touriest/{id}")
//	public ResponseEntity<Touristes> getTouriest(@PathVariable Integer id){
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/find/{id}";
//		
//		ResponseEntity<Touristes> resp=null;
//		Touristes tors= webClientBuilder.build()
//		.get()
//		.uri(serviceUrl,id)
//		.retrieve()
//		.bodyToMono(Touristes.class)
//		.block();
//		return resp=new ResponseEntity<Touristes>(tors,HttpStatus.ACCEPTED);
//	}
//	
//	@PostMapping("/Touriest")
//	public ResponseEntity<String> PostTouriest(@RequestBody Touristes touristes){
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/register";
//		System.out.println("Data Touriests"+touristes);
//		ResponseEntity<String> resp=null;
//		String response=webClientBuilder.build()
//		.post()
//		.uri(serviceUrl)
//		.body(BodyInserters.fromValue(touristes))
//		.retrieve()
//		.bodyToMono(String.class)
//		.block();	
//		return resp=new ResponseEntity<String>(response,HttpStatus.ACCEPTED);
//	}
//	
//	
//	@PutMapping("/Touriest")
//	public ResponseEntity<String> updateTouriest(@RequestBody Touristes touristes){
//		ResponseEntity<String> resp=null;
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/modify";
//		String response=webClientBuilder.build()
//		.put()
//		.uri(serviceUrl)
//		.body(BodyInserters.fromValue(touristes))
//		.retrieve()
//		.bodyToMono(String.class)
//		.block();
//		return resp=new ResponseEntity<String>(response,HttpStatus.ACCEPTED);
//	}
//	
//	
//	@DeleteMapping("/Touriest/{id}")
//	public ResponseEntity<String> deleteTouriest(@PathVariable Integer id){
//		ResponseEntity<String> resp=null;
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/delete/{id}";
//		String response= webClientBuilder.build()
//		.delete()
//		.uri(serviceUrl,id)
//		.retrieve()
//		.bodyToMono(String.class)
//		.block();
//		return resp=new ResponseEntity<String>(response,HttpStatus.ACCEPTED);
//	}
	
//	
//	@GetMapping("/users")
//	public ResponseEntity<?> getUserData() throws JsonMappingException, JsonProcessingException{
//		ResponseEntity<?> resp=null;
//		RestTemplate template=new RestTemplate();
//		String serviceUrl="https://gorest.co.in/public/v2/users";
//		ResponseEntity<String> response = template.exchange(serviceUrl, HttpMethod.GET, null,String.class);
//		String body = response.getBody();
//		ObjectMapper mapper=new ObjectMapper();
//		List<User> readValue = mapper.readValue(body, new TypeReference<List<User>>() {
//		});
//		System.out.println(body);
//		return resp=new ResponseEntity(readValue,HttpStatus.ACCEPTED);
//	}
//	
//	
//	@GetMapping("/user/{id}")
//	public ResponseEntity<?> getUser(@PathVariable Integer id) throws JsonMappingException, JsonProcessingException{
//		
//		Map<String,Object> map=new HashMap<>();
//		ResponseEntity<?> resp=null;
//		String serviceUrl="https://gorest.co.in/public/v2/users/{id}";
//		ResponseEntity<String> respones =template.exchange(serviceUrl, HttpMethod.GET, null,String.class,id);
//	         String body = respones.getBody();
//	         String name = respones.getStatusCode().name();
//	         HttpStatus statusCode = respones.getStatusCode();
//	         ObjectMapper mapper=new ObjectMapper();
////	 		Touristes readValue = mapper.readValue(jsonRes, Touristes.class);
//	         User readValue = mapper.readValue(body,User.class);
//	         map.put("user",readValue);
//	         map.put("codename", name);
//	         map.put("status", statusCode);
//		    return resp=new ResponseEntity(map,HttpStatus.ACCEPTED);
//		
//	}
//	
//	
//	@PostMapping("/user")
//	public ResponseEntity<String> updateUser(@RequestHeader(name = "Authorization", required = true) String Authorization,@RequestBody User user) throws JsonProcessingException{
//		
//		ObjectMapper mapper=new ObjectMapper();
//		String jsonData = mapper.writeValueAsString(user);
//		System.out.println(jsonData);
//		String serviceUrl="https://gorest.co.in/public/v2/users";
//		HttpHeaders headers=new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.set("Authorization",Authorization);
//		HttpEntity<String> entity=new HttpEntity<>(jsonData,headers);
//		ResponseEntity<String> resp= template.exchange(serviceUrl, HttpMethod.POST,entity,String.class);
//		String body=resp.getBody();
//		return resp=new ResponseEntity<String>(body,HttpStatus.CREATED);
//	}
	
	//get all Brand list
	

	
	
//	@GetMapping("/brands")
//	public ResponseEntity<?> AllTourests(@RequestHeader("Authorization") String token){
//		System.out.println(token);
//	
//		String serviceUrl="http://localhost:8585/brand/findAll";
//		ResponseEntity<?> resp=null;
//		Brand[] brands = webClientBuilder.build()
//		.get()
//		.uri(serviceUrl)
//		.header(HttpHeaders.AUTHORIZATION, token)
//		.retrieve()
//		.bodyToMono(Brand[].class)
//		.block();
//		return resp=new ResponseEntity<>(brands,HttpStatus.ACCEPTED);
//		
//	}
	
	
	@GetMapping("/search")
	public ResponseEntity<?> AllBrandSearch(@RequestHeader("Authorization") String token,
			@RequestParam (required = false) String name,
			@RequestParam (required = false) String BrandAlias,
			@RequestParam (required = false) String enabled){
		System.out.println(token);
	
		String serviceUrl="http://localhost:8585/brand";
		ResponseEntity<?> resp=null;
		Brand[] brands = WebClient.create(serviceUrl)
			    .get()
			    .uri(uriBuilder -> uriBuilder.path("/search")
			        .queryParam("name",name)
			        .queryParam("BrandAlias", BrandAlias)
			        .queryParam("enabled",enabled)
			        .build())
			        .header(HttpHeaders.AUTHORIZATION, token)
				    .retrieve()
				    .bodyToMono(Brand[].class)
				    .block();
	      Map<String, Object> response = new HashMap<>();
	      response.put("success",true);
	      response.put("data",brands);
		
		return resp=new ResponseEntity<>(response,HttpStatus.ACCEPTED);
		
	}
	


	
//	@GetMapping("/Touriest/{id}")
//	public ResponseEntity<Touristes> getTouriest(@PathVariable Integer id){
//		String serviceUrl="http://localhost:9901/RestMiniProject/tourist/find/{id}";
//		
//		ResponseEntity<Touristes> resp=null;
//		Touristes tors= webClientBuilder.build()
//		.get()
//		.uri(serviceUrl,id)
//		.retrieve()
//		.bodyToMono(Touristes.class)
//		.block();
//		return resp=new ResponseEntity<Touristes>(tors,HttpStatus.ACCEPTED);
//	}

}

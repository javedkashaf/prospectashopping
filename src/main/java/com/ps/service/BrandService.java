package com.ps.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ps.exception.BrandNotFoundException;
import com.ps.model.Brand;
import com.ps.model.Category;
import com.ps.model.MainBrand;
import com.ps.model.User;
import com.ps.repo.BrandRepository;
import com.ps.repo.MainBrandRepository;

@Service
public class BrandService {
	
	public static final int BRANDS_PER_PAGE=10;
	
	@Autowired
	private BrandRepository repo;
	
	@Autowired
	private MainBrandRepository mainBrand;
	
	public List<Brand> listAll() {
		return (List<Brand>) repo.findAll();
	}
	
	
//	public Optional<MainBrand> findById(Integer id) {
//        return  mainBrand.findById(id);
//    }
	
	
	public List<MainBrand> getBrandIds(Integer id) {
        return  mainBrand.BrandId(id);
    }
	
	//save the product
	public Brand save(Brand brand) {
		return repo.save(brand);
	}
	
	
	//get one id
	public Brand getoneBrand(Integer id) throws BrandNotFoundException {
		try {
			return repo.findById(id).get();
		} catch (NoSuchElementException ex) {
			throw new BrandNotFoundException("Could not find any brand with ID " + id);
		}
	}
	
	
	
	public void delete(Integer id) throws BrandNotFoundException {
		Long countById=repo.countById(id);
		if(countById == null || countById == 0) {
			throw new BrandNotFoundException("Could not find any brand with ID" + id);
		}
		repo.deleteById(id);
	}
	
	public boolean isUserBrand(Integer id) {
		boolean status = repo.existsById(id);
		return status;
	}
	
	
	public Brand UpdateBrand(Brand brand) {
		return repo.save(brand);
	}
	
	
	
	public String checkUniques(Integer id,String name) {
		boolean isCreatingNew = (id == null || id == 0);
		Brand brandByName = repo.findByName(name);
		if(isCreatingNew) {
			if(brandByName != null )return "Duplicate";
		}else {
			if(brandByName != null && brandByName.getId() != id) {
				return "Duplicate";
			}
		}
		return "OK";
	}
	
	
	
	public List<Brand> getAllSearchWithBoolean(String name, String BrandAlias, boolean enabled) {
		return repo.getAllBrandsWithBoolean(name, BrandAlias, enabled);
	}


	public List<Brand> getAllSearch(String name, String BrandAlias) {
		return repo.getAllBrands(name, BrandAlias);
	}



	
}

package com.ps.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ps.exception.ProductNotFoundException;
import com.ps.model.Product;
import com.ps.model.User;

public interface ProductService {
	
	public Product SaveProduct(Product product);
	

//	public List<Product> getAllProducts(String name,String alias,float cost,float price);
	
	public List<Product> products();
	
	public Product getOneProduct(Integer id);
	
	public void deleteById(Integer id);
	
	boolean  isProductExist(Integer id); 
	
	public Integer updateProduct(Product product);
	
	
	//activate of the produt
//	int updateProductStatus(boolean status, Integer id);
	
	
	//for delete multiple records by checks
	public List<Product> getByMultipleIds(Integer[] ids);
	
	//check uniquens of the product
	public String checkUniques(String name);
	
	public void EnabledAndDisbaledProduct(Integer id,boolean enabled);
	
	public void deleteProduct(Integer id) throws ProductNotFoundException;
	
	public void saveProductPrice(Product productInform);
	
	public Page<Product> getAllSearchProducts(String name,String brand,int pageNo,int pageSize);



	

}
